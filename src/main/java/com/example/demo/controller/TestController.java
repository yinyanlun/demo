package com.example.demo.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
@RequestMapping("/test")
public class TestController {



    @RequestMapping("/logs")
    public String test(String name) {
        log.info("测试:"+name);
        log.error("测试:"+name);
        return "test："+name;
    }
}
